$(document).ready(function () {

    $('.menu').on('click', function () {
        $(this).find('span').toggleClass('white-menu');
        $('.menu-open').toggleClass('active');
    });
    $('.question-more').on('click', function ( e ) {
        let target = $( this ),
            parent = target.closest( '.question-item' ),
            toggleBlock = parent.find( '.question-answer' );
         parent.toggleClass( '__open' );
         toggleBlock.toggle( 'fast' );
        // $(this).parent().find('.question-answer').toggleClass('clicked');

    })

    var swiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.button-next',
            prevEl: '.button-prev',
        },
    });
});